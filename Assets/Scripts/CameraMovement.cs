﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float panSpeed = 20;
    public float panBorderThickness = 10;
    public float scrollSpeed = 500;

    // Update is called once per frame
    void Update()
    {
        Vector3 camPos = transform.position;

        if (Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            camPos.x -= panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.y <= panBorderThickness)
        {
            camPos.x += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            camPos.z += panSpeed * Time.deltaTime;
        }
        if (Input.mousePosition.x <= panBorderThickness)
        {
            camPos.z -= panSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");

        camPos.y -= scroll * scrollSpeed * Time.deltaTime;

        camPos.x = Mathf.Clamp(camPos.x, 28, 155);
        camPos.y = Mathf.Clamp(camPos.y, 10, 25);
        camPos.z = Mathf.Clamp(camPos.z, 10, 140);

        transform.position = camPos;
    }
}
