﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    protected Transform target;

    public float bulletDamage;
    public float speed;

    protected Vector3 dir;
    protected float dis;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }

        dir = target.position - transform.position;
        dis = speed * Time.deltaTime;

        if (dir.magnitude <= dis)
        {
            if (target.GetComponent<UnitStats>()) target.GetComponent<UnitStats>().TakeDamage(bulletDamage);
            else if (target.GetComponent<TowerStats>()) target.GetComponent<TowerStats>().TakeDamage(bulletDamage);
            Destroy(this.gameObject);
        }

        transform.Translate(dir.normalized * dis, Space.World);
    }

    public virtual void Seek(Transform _target)
    {
        target = _target;
    }

    public virtual void SetDamage(float damage)
    {
        bulletDamage = damage; 
    }
}
