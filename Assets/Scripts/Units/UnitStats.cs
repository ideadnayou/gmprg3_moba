﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitType
{
    Melee_Creep,
    Range_Creep,
    Siege_Creep,
    Super_Creep,
}

public class UnitStats : MonoBehaviour
{
    #region Unit Data
    public UnitType unitType;
    public LayerMask enemyLayer;

    public float maxHealth;
    public float currentHealth;

    public float unitDamage;

    public float unitDetectRange;
    public float unitAtkRange;

    public float unitArmor;
    public float unitMagicResist;

    public float unitMoveSpeed;

    public float unitAtkSpeed;
    public float unitAtkCD = 0;

    public bool isDead;
    #endregion

    #region Unit Bounty
    public int exp;
    public int gold;
    #endregion

    // Start is called before the first frame update
    protected virtual void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            isDead = true;
            Destroy(gameObject, 5);
        }
    }

    public virtual void TakeDamage(float damage)
    {
        currentHealth -= damage;
    }

    public virtual void UpgradeUnitStats()
    {

    }
}
