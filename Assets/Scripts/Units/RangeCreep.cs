﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeCreep : UnitStats
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
    }

    public override void UpgradeUnitStats()
    {
        maxHealth += 12;
        unitDamage += 2;
        gold += 6;
        exp += 6;
    }
}
