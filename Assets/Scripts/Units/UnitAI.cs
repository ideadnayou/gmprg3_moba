﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum aiState
{
    FollowPath_State,
    ChaseEnemy_State,
    AttackEnemy_State,
    ReturnToPoint_State
}

public class UnitAI : MonoBehaviour
{
    #region Pathway Variables
    public List<Transform> pathPoints;
    public Transform currentPoint;
    #endregion

    public UnitStats stats;

    public aiState currentState;

    public Transform target;

    public float maxDisFromPoint;
    public float minDisFromPoint;

    [SerializeField]
    protected List<Transform> targetList = new List<Transform>();

    protected NavMeshAgent agent;

    protected int index = 0;

    protected Animator anim;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = stats.unitMoveSpeed;
        currentState = aiState.FollowPath_State;
        currentPoint = pathPoints[0];
        agent.SetDestination(currentPoint.position);

        InvokeRepeating("SearchForTargets", 0, 1);

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        float disFromPoint = Vector3.Distance(transform.position, currentPoint.position);

        //if (disFromPoint > maxDisFromPoint && currentState != aiState.FollowPath_State) currentState = aiState.ReturnToPoint_State;

        Debug.Log(currentState);

        switch (currentState)
        {
            case aiState.FollowPath_State:
                FollowPath(disFromPoint);
                break;

            case aiState.ChaseEnemy_State:
                ChaseEnemy();
                break;

            case aiState.AttackEnemy_State:
                AttackEnemy();
                break;

            case aiState.ReturnToPoint_State:
                ReturnToPath(disFromPoint);
                break;
        }
    }

    // Search for enemy targets using OverlapSphere
    protected virtual void SearchForTargets()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, (float)stats.unitDetectRange, stats.enemyLayer);

        AddTargetToList(hitColliders);

        if (targetList.Count > 0 && currentState != aiState.ReturnToPoint_State)
        {
            currentState = aiState.AttackEnemy_State;
        }
    }

    // Sort list by the nearest target
    protected virtual void SortList(List<Transform> items)
    {
        items.Sort(delegate (Transform t1, Transform t2)
        {
            return Vector3.Distance(t1.transform.position, transform.position).CompareTo(Vector3.Distance(t2.transform.position, transform.position));
        });
    }

    protected virtual void AddTargetToList(Collider[] col)
    {
        foreach (Collider unit in col)
        {
            if (unit.GetComponent<UnitStats>())
            {
                targetList.Add(unit.gameObject.transform);
            }
            else if (unit.GetComponent<TowerStats>())
            {
                if (unit.GetComponent<TowerStats>().isInvulnerable) return;
                else targetList.Add(unit.gameObject.transform);
            }
            else if (unit.GetComponent<HeroStats>())
            {
                targetList.Add(unit.gameObject.transform);
            }
        }
    }

    protected virtual void FollowPath(float distance)
    {
        agent.isStopped = false;
        anim.SetBool("walking", true);

        // Set the next destination point after reaching current destination point
        if (distance <= minDisFromPoint && target == null)
        {
            // Set index to not go out of range
            if (index < pathPoints.Count - 1) index++;

            currentPoint = pathPoints[index];
            agent.SetDestination(currentPoint.position);
        }        
    }

    protected virtual void ChaseEnemy()
    {
        agent.isStopped = false;
        anim.SetBool("walking", true);
        agent.SetDestination(target.position);

        float dis = Vector3.Distance(target.position, transform.position);

        float disOfEnemyFromPoint = Vector3.Distance(target.position, currentPoint.position);

        if (dis <= stats.unitAtkRange)
        {
            currentState = aiState.AttackEnemy_State;
        }

        // Check if target is far from current point location
        else if (disOfEnemyFromPoint > maxDisFromPoint) currentState = aiState.ReturnToPoint_State;
    }

    protected virtual void AttackEnemy()
    {
        agent.isStopped = true;
        anim.SetBool("walking", false);
        SortList(targetList);

        float disFromEnemy = Vector3.Distance(targetList[0].position, transform.position);

        // Set target;
        if (disFromEnemy <= stats.unitDetectRange && target == null)
        {
            target = targetList[0];
        }

        if (disFromEnemy <= stats.unitAtkRange) EvaluateAttack(target);

        // If target is dead return to path
        if (target != null && target.GetComponent<UnitStats>())
        {
            if (target.GetComponent<UnitStats>().isDead)
            {
                target = null;
                targetList.Clear();

                currentState = aiState.ReturnToPoint_State;
            }
        }
        else if (target != null && target.GetComponent<TowerStats>())
        {
            if (target.GetComponent<TowerStats>().isDead)
            {
                target = null;
                targetList.Clear();

                currentState = aiState.ReturnToPoint_State;
            }
        }
        else if (target != null && target.GetComponent<HeroStats>())
        {
            if (target.GetComponent<HeroStats>().isDead)
            {
                target = null;
                targetList.Clear();

                currentState = aiState.ReturnToPoint_State;
            }
        }

        // If target is out of range
        if (disFromEnemy > stats.unitAtkRange) currentState = aiState.ChaseEnemy_State;
    }

    protected virtual void ReturnToPath(float distance)
    {
        anim.SetBool("walking", true);
        agent.isStopped = false;
        // Clear target and list so that unit wont attack
        target = null;
        targetList.Clear();

        // Get unit to go back to destination
        agent.SetDestination(currentPoint.position);

        // Set state to path following once near current point
        if (distance < minDisFromPoint) currentState = aiState.FollowPath_State;
    }

    protected virtual void EvaluateAttack(Transform _target)
    {
        if (stats.unitAtkCD <= 0)
        {
            anim.SetTrigger("attack");
            stats.unitAtkCD = 1f / stats.unitAtkSpeed;

            if (_target.GetComponent<UnitStats>())
            {
                _target.GetComponent<UnitStats>().TakeDamage(stats.unitDamage);
            }
            else if (_target.GetComponent<TowerStats>())
            {
                _target.GetComponent<TowerStats>().TakeDamage(stats.unitDamage);
            }
            else if (_target.GetComponent<HeroStats>())
            {
                _target.GetComponent<HeroStats>().TakeDamage(stats.unitDamage);
            }
        }

        stats.unitAtkCD -= Time.deltaTime;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, stats.unitAtkRange);

        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, stats.unitDetectRange);
    }
}
