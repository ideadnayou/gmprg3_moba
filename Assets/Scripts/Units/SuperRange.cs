﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperRange : UnitStats
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
    }

    public override void UpgradeUnitStats()
    {
        maxHealth += 18;
        unitDamage += 3;
        gold += 6;
    }
}
