﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperMelee : UnitStats
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
    }

    public override void UpgradeUnitStats()
    {
        maxHealth += 19;
        unitDamage += 2;
        gold += 2;
    }
}
