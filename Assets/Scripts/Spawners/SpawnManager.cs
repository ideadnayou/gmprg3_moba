﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public List<Transform> pathPoints;

    public Transform spawnPoint;

    public float spawnTimer;
    public int waveCount;

    public bool matchStarted = true;

    public List<GameObject> currentWave;

    public List<GameObject> enemyBarracks;

    #region Creep Wave Lists
    public List<GameObject> normalWave;
    public List<GameObject> siegeWave;
    public List<GameObject> superWave;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        spawnTimer += Time.deltaTime;
    }

    void SpawnWave()
    {
        foreach (GameObject unit in currentWave)
        {
            Instantiate(unit, spawnPoint.position, spawnPoint.rotation);
        }
    }
}
