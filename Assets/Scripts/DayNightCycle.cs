﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    public float cycleTimer;
    public Light gameLight;

    public bool dayTime = true;

    public float lerpTime;

    Color colorChange;

    // Start is called before the first frame update
    void Start()
    {
        colorChange = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        cycleTimer += Time.deltaTime;
        lerpTime += Time.deltaTime;

        if (cycleTimer >= 300)
        {
            if (dayTime)
            {
                colorChange = new Color32(90, 140, 255, 255);
                dayTime = !dayTime;
                cycleTimer = 0;
            }
            else if (!dayTime)
            {
                colorChange = Color.white;
                dayTime = !dayTime;
                cycleTimer = 0;
            }            
        }

        if (Input.GetKeyDown(KeyCode.Space)) Time.timeScale = 50;

        if (Input.GetKeyUp(KeyCode.Space)) Time.timeScale = 1;

        gameLight.color = Color.Lerp(gameLight.color, colorChange, cycleTimer / 100);
    }
}
