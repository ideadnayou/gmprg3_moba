﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    public Text gameTimeText;
    public float gameTimer;
    public int seconds;
    public int minutes;

    // Update is called once per frame
    void Update()
    {
        gameTimer += Time.deltaTime;
        seconds = (int)(gameTimer % 60);
        minutes = (int)(gameTimer / 60) % 60;

        string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);

        gameTimeText.text = timerString;
    }
}
