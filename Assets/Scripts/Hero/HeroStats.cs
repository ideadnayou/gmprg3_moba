﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroStats : MonoBehaviour
{
    public float maxHealth;
    public float currentHealth;
    public float healthRegen;

    public float maxMana;
    public float currentMana;
    public float manaRegen;

    public float armor;
    public float moveSpeed;

    public float atkSpeed;
    public float atkRange;
    public float detectRange;

    public GameObject bullet;

    public bool isDead;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(float damage)
    {

    }
}
