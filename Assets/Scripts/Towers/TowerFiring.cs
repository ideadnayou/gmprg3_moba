﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFiring : MonoBehaviour
{
    public Transform firePoint;

    public GameObject bullet;

    public float fireRate;
    public float fireCD = 0;

    public TowerStats stats;
    public TowerTargeting target;


    // Start is called before the first frame update
    void Start()
    {
        stats = this.GetComponent<TowerStats>();
        target = this.GetComponent<TowerTargeting>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.enabled)
        {
            if (fireCD <= 0)
            {
                Shoot();
                fireCD = 1f / fireRate;
            }

            fireCD -= Time.deltaTime;
        }
        else if (!this.enabled)
        {
            fireCD = 0;
        }
    }

    void Shoot()
    {
        GameObject projectile = Instantiate(bullet, firePoint.position, Quaternion.identity);
        projectile.GetComponent<BaseProjectile>().SetDamage(stats.towerDamage);
        projectile.GetComponent<BaseProjectile>().Seek(target.target);
    }
}