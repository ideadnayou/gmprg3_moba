﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TowerTier
{
    Tier_1,
    Tier_2,
    Tier_3,
    Ancient
}

public class TowerStats : MonoBehaviour
{
    public float towerHealth;
    public float currentTowerHealth;

    public float towerArmor;
    public float towerMagicResist;

    public TowerTier towerTier;
    public LayerMask enemyLayer;

    public float towerRange;
    public float towerDamage;

    public bool isDead;
    public bool isInvulnerable;

    public List<GameObject> gameObjects;

    public int teamGoldBounty;
    public int lastHitGoldBounty;

    // Start is called before the first frame update
    void Start()
    {
        currentTowerHealth = towerHealth;
    }

    // Update is called once per frame
    void Update()
    {
        switch (towerTier)
        {
            case TowerTier.Ancient:
                CheckListForAnyDestroyed();
                break;
            default: CheckList();
                break;
        }

        if (gameObjects.Count > 0) isInvulnerable = true;
        else isInvulnerable = false;

        if (currentTowerHealth <= 0)
        {
            currentTowerHealth = 0;
            isDead = true;

            Destroy(gameObject, 5);
        }

    }

    public void TakeDamage(float damage)
    {
        currentTowerHealth -= damage;
    }

    void CheckList()
    {
        foreach (GameObject obj in gameObjects)
        {
            if (obj.GetComponent<TowerStats>().isDead) gameObjects.Remove(obj);
        }
    }

    void CheckListForAnyDestroyed()
    {
        foreach (GameObject obj in gameObjects)
        {
            if (obj.GetComponent<TowerStats>().isDead) isInvulnerable = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, towerRange);
    }
}
