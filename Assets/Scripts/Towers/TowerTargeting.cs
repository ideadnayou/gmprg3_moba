﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTargeting : MonoBehaviour
{
    public Transform target;
    TowerStats stats;
    TowerFiring fire;

    [SerializeField]
    List<Transform> targetList = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        stats = this.GetComponent<TowerStats>();
        fire = this.GetComponent<TowerFiring>();
        fire.enabled = false;

        InvokeRepeating("SearchForEnemy", 0, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            fire.enabled = false;
            return;
        }
        else if (target != null) fire.enabled = true;
    }

    void SearchForEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, stats.towerRange, stats.enemyLayer);

        AddTargetToList(hitColliders);

        if (targetList.Count > 0)
        {
            sortList(targetList);

            target = targetList[0];

            float disFromTarget = Vector3.Distance(targetList[0].position, transform.position);

            if (disFromTarget >= stats.towerRange)
            {
                target = null;
                targetList.Clear();
            }

            if (target != null && target.GetComponent<UnitStats>())
            {
                if (target.GetComponent<UnitStats>().isDead)
                {
                    target = null;
                    targetList.Clear();
                }
            }
            else if (target != null && target.GetComponent<HeroStats>())
            {
                if (target.GetComponent<HeroStats>().isDead)
                {
                    target = null;
                    targetList.Clear();
                }
            }
        }
    }

    void sortList(List<Transform> items)
    {
        items.Sort(delegate (Transform t1, Transform t2)
        {
            return Vector3.Distance(t1.transform.position, transform.position).CompareTo(Vector3.Distance(t2.transform.position, transform.position));
        });
    }

    void AddTargetToList(Collider[] col)
    {
        foreach (Collider unit in col)
        {
            if (unit.GetComponent<UnitStats>() || unit.GetComponent<HeroStats>()) targetList.Add(unit.gameObject.transform);
        }
    }
}
